#!/usr/bin/python3
import sys
import requests
import re
import os.path
import markdown
from bs4 import BeautifulSoup
from markdownify import markdownify as md
from md2gemini import md2gemini

# Note: This is intended to scrape my own posts from a dynamic site so I can
# put them into a md-based static generator. It's not intended to be general.
# Feel free to adapt it to your use, though!

# TODO
# Detect image paths
# Expand tags from camelCase to Title Case
# Remove hashtags from description
# Add tags from hashtags
# Set up placeholder for Flickr?

sys.argv.pop(0) # remove the script name from the parameters we want to look at
output_format = 'md'
slug = ''
category = ''
url = ''
slug_override=False
original='keep'
while (sys.argv):
  nextItem = sys.argv.pop(0)
  print( "checking " + nextItem)
  if (nextItem in ['md', 'gem', 'gemlog', 'html']):
    output_format = nextItem
  elif (nextItem in ['move','keep','canon']):
    original = nextItem
  elif (nextItem.startswith('cat=')):
    category = nextItem[4:]
  elif (nextItem.startswith('https://') or nextItem.startswith('http://')):
    url = nextItem
  else:
    slug = nextItem
    slug_override = True
page = requests.get(url)
soup = BeautifulSoup(page.content, 'html.parser')

##########################################################
def decompose_if_exists(soup):
  if(soup):
    soup.decompose()
##########################################################

# Extract slug from URL
if (not slug):
  base_url = re.sub("\?.*$", "", url)
  slug_match = re.search("/([a-zA-Z0-9_-]+)(/|.html)?$", base_url)
  if (slug_match):
    slug = slug_match.group(1)
  else:
    slug="unknown"

# Extract metadata and article content from page. Prefer Microformats2, fall back to others. 

title = ( soup.find(class_='p-name') or soup.find(class_='entry-title') or soup.find('photo-title') or soup.find('h1') or soup.find('title') ).text.strip()
title = re.sub('#','', title)



date_elem = soup.find(class_='dt-published') or soup.find(class_='entry-date') or soup.find(class_='published') or soup.find('meta', property="og:published_time")
date_published = ''
if (date_elem):
  if 'datetime' in date_elem.attrs:
    date_published = date_elem['datetime']
  elif 'value' in date_elem.attrs:
    date_published = date_elem['value']
  elif 'content' in date_elem.attrs:
    date_published = date_elem['content']

# Get YYYY-MM-DD from YYYY-MM-DDT12:34:56etc TODO: Actually parse the date
date_simple=''
if (date_published):
  date_simple = date_published[:10]

tag_elems = list( set( soup.find_all('a', class_='p-category') + soup.find_all('a', rel='tag') ) )
tags = [t.text for t in tag_elems]
# TODO separate tags from categories so we can leave "Instagram" on a post *about* Instagram but clear it from a post *from* Instagram. (My fault on the tag choices)
cleanup = [
	'Photo on Blog', 'Photo on Flickr', 'Photo (check)', 'Imported to Blog', 'Cross-Posted on Blog',
	'iNaturalist', 'Twitter', 'Instagram', 'PixelFed'
	]
for c in cleanup:
  if(c in tags):
    tags.remove(c)
#TODO add spaces and convert to title case

description_elem = soup.find('meta', attrs={'name': 'description'}) or soup.find('meta', property='og:description')

article = soup.find(class_='e-content') or soup.find(class_='entry-content') or soup.find('article') or soup.find(id='article') or soup.find(class_='article') or soup.find(id='mw-content-text') or soup.find(class_='mw-body-content') or soup.find(class_='title-desc-block') or BeautifulSoup("<article></article>", features='lxml')

# TODO extract author from source instead
author = 'Kelson Vibber'

# Look for images within the article and download them.
# First image becomes the main one.
main_image=''
main_alt=''
images = article.find_all('img', 'lxml')
# TODO find_all and get multiple OG images (useful for Mastodon with multiple attachments)
og_image_elem = soup.find('meta', property='og:image')
og_image_alt = soup.find('meta', property='og:image:alt')
suffix = ''

# Get the OpenGraph images
if (og_image_elem and not images):
  images = []
  image_url = og_image_elem['content']
  image_alt = 'TODO: ADD DESCRIPTION'
  if (og_image_alt):
    image_alt = og_image_alt['content']
  # Yes, there are better ways to do this. They can wait until I refactor.
  image = BeautifulSoup("<img/>", features="lxml")
  image['src'] = image_url
  image['alt'] = image_alt
  images = [image]

for image in images:
  if (image['src']):
    image_url = image['src']
    # If the image URL ends in a width/height pair (ex. 1024x1024), strip it out so we can grab the full image.
    image_full = re.sub(r"(?:-[0-9]+x[0-9]+)\.([a-z]+)$", r".\1", image_url)
    image_full = re.sub('_thumb', '', image_full)
    image_filename = image_full.split('/')[-1]
    image_extension = image_filename.split('.')[-1]
    
    # If the filename is generated, build a cleaner one from the post slug.
    # Starts with 5 numbers, 25 alphanumeric, or tumblr
    filename_match = re.search("^(?:[0-9]{5,}|\w{25,}|tumblr)", image_filename)
    # First 8 chars has both letters and numbers and nothing else
    filename_first8 = image_filename[0:8]
    filename_stew = filename_first8.isalnum()
    if (filename_stew):
      filename_stew_match = re.search("[0-9]", filename_first8)
    else:
      filename_stew_match = False
    if (filename_match or filename_stew_match):
      image_filename = f"{slug}{suffix}.{image_extension}"
      if (suffix):
        suffix = suffix + 1
      else:
        suffix = 2

    # Set the first image we find as the main image (for thumbnails, photo posts etc)
    if (not main_image):
      main_image = image_filename
      main_alt = image['alt']
    
    # If we don't already have the file, try to retrieve it.
    if (not os.path.exists(image_filename)):
      # Try to grab the full image. If it doesn't exist, retry with the original URL.
      print ("Retrieving " + image_full)
      img = requests.get(image_full)
      if ( (not img) and image_url != image_full ):
        print ("Retrying with " + image_full)
        img = requests.get(image_url)
      # Save the image if the file doesn't already exist
      if (img):
        print("Saving " + image_filename)
        with open(image_filename, 'wb') as img_file:
          img_file.write(img.content)
          image['src'] = image_filename


# Look for syndication links within the article. Remove them from the document tree, but keep the data for later.
syndication_elems = list( set( soup.find_all('a', class_='sourcelink') + soup.find_all('a', class_='u-syndication') ) )
# TODO clean up the labels generated by the Indieweb Syndication Links plugin
for link in syndication_elems:
  link.extract()

# Clean up cruft such as wordpress.com ads, related posts links, etc.
decompose_if_exists( article.find('style') )
decompose_if_exists( article.find('script') )
decompose_if_exists( article.find(class_='wordads-ad-wrapper') )
decompose_if_exists( article.find(id='jp-post-flair') )
decompose_if_exists( article.find(id='jp-relatedposts') )
decompose_if_exists( article.find(class_='crp_related') )

# Unlink hashtags in source.
for link in (article.find_all('a')):
  if link.text and link.text[0] == '#':
    link.unwrap()

# Pick name for link to source in front matter
original_name = "Original"
if (url.__contains__('hyperborea.org/journal')):
  original_name = "K2R (Blog)"
elif (url.__contains__('kelson.wordpress.com')):
  original_name = "Parallel Lines"
elif (url.__contains__('wordpress.com')):
  original_name = "WordPress"
elif (url.__contains__('wandering.shop') or url.__contains__('photog.social')):
  original_name = "Mastodon"
elif (url.__contains__('socialposts.kv')):
  original_name = "Skip"
# Note: Twitter and Pixelfed are all JavaScript and can't be scraped, though I'm working on using OG data.
elif (url.__contains__('twitter.com')):
  original_name = "Twitter"
elif (url.__contains__('pixelfed.social')):
  original_name = "Pixelfed"

if (title == "KelsonV shared a post" and description_elem):
  title = description_elem['content']



# Add metadata to front matter
front_matter = "---\n"
if (title):
  esc_title =  re.sub('"', '\\"', title) 
  front_matter += f"title: \"{esc_title}\"\n"
if (date_published):
  front_matter += f"date: \"{date_published}\"\n"
# Description may have quotation marks
if (description_elem):
  desc = re.sub('"', '\\"', description_elem['content']) 
  front_matter += f"description: \"{desc}\"\n"
if (category):
  front_matter += f"category: \"{category}\"\n"
if (tags):
  front_matter += f"tags:\n"
  for tag in tags:
    front_matter += f"  - \"{tag}\"\n"
if (main_image):
  front_matter += f"image: {main_image}\n"
  front_matter += f"alt: \"{main_alt}\"\n"
if (original_name != 'Skip'):
  if (original == 'canon'):
    front_matter += f"canonical: \"{url}\"\n"
  if (original == 'move'):
    front_matter += f"movedFrom:\n  \"{original_name}\": \"{url}\"\n"

if (syndication_elems or (original == 'keep' and original_name != 'Skip') ):
  front_matter += f"alternates:\n"
if ( original != 'move' and original_name != 'Skip'):
  front_matter += f"  \"{original_name}\": \"{url}\"\n"
if (syndication_elems):
  for link in syndication_elems:
    label = re.sub(r"^On ", "", link.text)
    label = re.sub("PixelFed.Social", "PixelFed", label)
    label = label.strip()
    front_matter += f"  \"{label}\": \"{link['href']}\"\n"
front_matter += f"---\n"

# Convert to markdown, clean up double spacing, and remove ending whitespace
marked = md(str(article), heading_style='ATX')
# Fall back to description if it's present and the article isn't
if (description_elem and not marked):
  marked=md(description_elem['content'])
marked = re.sub(r'\n\n', r'\n', marked , flags=re.M)
marked = marked.strip()

# Show front matter
print (front_matter)

# Format as basic gemtext, gemlog, or attach front matter to markdown
# Todo add tags/categories to gemlog footer
# Todo convert nice quotes/apostrophes/etc back to ascii
if(output_format == 'gem'):
  file_name = f"{slug}.gmi"
  output = f"# {title}\n\n" + md2gemini(marked, links='copy', plain=True, strip_html=True) + f"""

{date_simple}

=> {url} Web version at {original_name}"
"""
elif(output_format == 'gemlog'):
  file_name = f"{date_simple}-{slug}.gmi"
  output = f"# {title}\n\n" + md2gemini(marked, links='copy', plain=True, strip_html=True) + f"""

—{author}, {date_simple}

=> ./ Previous
=> ./ Next

=> {url} Web
"""
elif(output_format == 'html'):
  # Convert markdown back to simplified HTML
  file_name = f"{slug}.html"
  output = markdown.markdown(marked)

else:
  # TODO optionally replace image code with shortcode for 11ty theme
  file_name = f"{slug}.md"
  output = front_matter + marked

# Save to file
if (not os.path.exists(file_name)):
  with open(file_name, 'w') as save_file:
    save_file.write(output)
    print (f"Saved: {file_name}")
else:
  print (f"ERROR: {file_name} already exists")


